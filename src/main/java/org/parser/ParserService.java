package org.parser;

import org.parser.config.ParserConfiguration;
import org.parser.resource.ProcurementResource;

import com.yammer.dropwizard.Service;
import com.yammer.dropwizard.config.Bootstrap;
import com.yammer.dropwizard.config.Environment;

public class ParserService extends Service<ParserConfiguration> {

	@Override
	public void initialize(Bootstrap<ParserConfiguration> bootstrap) {

	}

	@Override
	public void run(ParserConfiguration configuration, Environment environment)
			throws Exception {

		environment.addResource(ProcurementResource.class);

	}

	public static void main(String[] args) throws Exception {
		new ParserService().run(new String[] { "server",
				"src/main/resources/config.yml" });
	}

}
