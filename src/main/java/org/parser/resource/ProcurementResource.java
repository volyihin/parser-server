package org.parser.resource;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONObject;
import org.parser.entity.Procurement;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.sun.research.ws.wadl.Response;
import com.yammer.metrics.annotation.Timed;

@Path("/procurements")
@Produces(value = MediaType.APPLICATION_JSON)
@Consumes(value = MediaType.APPLICATION_JSON)
public class ProcurementResource {
	
	
	@GET
	@Timed
	public List<Procurement> getProcurements() throws IOException {
		
		// These code snippets use an open-source library. http://unirest.io/java
		try {
			HttpResponse<JsonNode> response = Unirest.get("https://clearspending.p.mashape.com/v1/contracts/search/?&page=1&perpage=50&placing=5&returnfields=%5Bprice%2CregNum%2Cproducts%5D")
					.header("X-Mashape-Key", "Tku5NGVvE6mshcUMa289A2XGbwpSp1HgXd8jsnez5kfswPSocu")
					.asJson();
			System.out.println(response.getRawBody());
			Unirest.shutdown();
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		

		
		return Lists.newArrayList();
		
	}
	
	
	public static void main(String[] args) throws IOException {
		
		new ProcurementResource().getProcurements();
	}

}
